# cidemon
### Demonstration project about how CI works.

Continuous Integration (CI) is a development practice that requires developers to integrate code into a shared repository several times a day. Each check-in is then verified by an automated build, allowing teams to detect problems early.
***
### Existing systems
- [Travis](https://travis-ci.org/)
- [Jenkins](https://jenkins.io/)
- [Gitlab CI & CD](https://about.gitlab.com/product/continuous-integration/)

---
### Medium description about CD

[click](https://medium.com/@edzob/ci-and-cd-in-the-wild-b5ca8f71fa28)
---
### Gitlab CI with simple stuffs
```
runer - the program installed on pc or server, which will
build your project and do other cool stufs e.g. "rm -rf / "

project - a source code which will be integrated(builded and tested)

.gitlab-ci.yml - file with instruction, how to build your project
```
![runner look with dummy words](./docimgs/runnerimagination.png)
#
#### Little how to & docs
- [Runner](https://docs.gitlab.com/runner/)
- [How to setup runner](https://docs.gitlab.com/runner/register/index.html)
- [== Advanced setup runner ==](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)
- [GNU/LINUX runner register](https://docs.gitlab.com/runner/register/index.html#gnulinux)
- [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/)

##### FYI how toml looks like with succesfull settings
![tomml](./docimgs/tomml_example.png)
#
#### Step by step guide
1. **Repository settings.** (Only ADMIN or Maintainer can do this)
   
   * Runner settings
   
       1. open your repository settings
    
          ![settins img](./docimgs/settings.png)
       2. get runner
          ![runner img](./docimgs/runnerclikc.png)
       3. Remeber info & disable shared runners
          ![runnerinfo](./docimgs/runnerinfo.png)
2. **Runner settings**
   * [install runner on the server & settup runner](https://docs.gitlab.com/runner/register/index.html#gnulinux)
   * create .gitlab-ci.yml 
3. **If your runner working**

   if you can see something like that, it's mean you are cool!
   ![runner started](./docimgs/succesfull.png)